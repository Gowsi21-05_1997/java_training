package com.service;

import com.model.College;
import com.model.Department;
import com.model.Student;

public class DepartmentServiceImpl implements DepartmentService {

	public Student searchStudent(Department department, int search) {
		Student dummy = null;
		Student[] studentArray = department.getStudent();

		for (int i = 0; i < studentArray.length; i++) {
			if (studentArray[i].getStuId() == search) {

				dummy = studentArray[i];

			} else {
				
			}

		}
		return dummy;
	}

	public Department searchStudentByCollege(College college, int search) {
		Department dummy = null;
		Department[] collegeArray = college.getDepartment();

		for (int i = 0; i < collegeArray.length; i++) {
			if (collegeArray[i].getDepId() == search) {

				dummy = collegeArray[i];

			} else {
				
			}

		}
		return dummy;
	}

}
