package com.service;

import com.model.College;
import com.model.Department;
import com.model.Student;

public interface DepartmentService {
	public abstract Student searchStudent(Department department, int search);
	
	public abstract Department searchStudentByCollege(College college, int search);
}
