package com.main;

import com.model.College;
import com.model.Department;
import com.model.Student;
import com.service.DepartmentService;
import com.service.DepartmentServiceImpl;

public class EducationApp {

	public static void main(String[] args) {
		Student student1 = new Student(1, "Akshaya", 20);
		Student student2 = new Student(2, "Akshara", 20);
		Student student3 = new Student(3, "Ajay", 20);
		Department dept = new Department();
		dept.setDepId(1);
		dept.setDeptName("CSE");
		Student[] students = new Student[3];
		students[0] = student1;
		students[1] = student2;
		students[2] = student3;
		dept.setStudent(students);
		College college = new College();
		Department department1 = new Department(1, "CSE", students);
		Department department2 = new Department(2, "ECE", students);
		Department department3 = new Department(3, "MECH", students);
		Department[] deepDepartments = new Department[3];
		deepDepartments[0] = department1;
		deepDepartments[1] = department2;
		deepDepartments[2] = department3;

		college.setStudent(students);
		college.setDepartment(deepDepartments);
		DepartmentService departmentService = new DepartmentServiceImpl();
		Student student = departmentService.searchStudent(dept, 2);
		if (student != null) {
			System.out.println("STUDENT ID:" + student.getStuId());
			System.out.println("STUDENT NAME:" + student.getName());
			System.out.println("STUDENT AGE:" + student.getAge());

		}
		Department collStudent = departmentService.searchStudentByCollege(college, 1);
		if (student != null) {
			System.out.println("DEPARTMENT ID:" + collStudent.getDepId());
			System.out.println("DEPARTMENT NAME:" + collStudent.getDeptName());
		

		}
	}

}
