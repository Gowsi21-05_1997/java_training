package com.main;

import com.model.Employee;
import com.service.EmployeeService;
import com.service.EmployeeServiceImpl;

public class EmployeeApp {

	public static void main(String[] args) {
		Employee employee1 = new Employee(1, "Abi");
		Employee employee2 = new Employee(2, "Akshaya");

		Employee[] employees = new Employee[2];
		employees[0] = employee1;
		employees[1] = employee2;
		EmployeeService employeeService = new EmployeeServiceImpl();
		Employee result1 = employeeService.searchById(employees, 1);
		if (result1 != null) {
			System.out.println("Employee Id :" + result1.getEmployeeId());
			System.out.println("Employee Name:" + result1.getEmployeeName());
		}
		Employee[] result2 = employeeService.searchByName(employees, "Akshaya");
		if (result2.length > 0) {
			for (int i = 0; i < result2.length; i++) {
				if (result2[i] != null) {
					System.out.println("Employee Id :" + result2[i].getEmployeeId());
					System.out.println("Employee Name:" + result2[i].getEmployeeName());
				}
			}
		}
	}

}
