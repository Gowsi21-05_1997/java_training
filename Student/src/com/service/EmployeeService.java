package com.service;

import com.model.Employee;

public interface EmployeeService {
	public abstract Employee searchById(Employee[] employeeArray, int employeeId);

	public abstract Employee[] searchByName(Employee[] employeeArray, String employeeName);

}
