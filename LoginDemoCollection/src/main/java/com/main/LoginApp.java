package com.main;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.model.User;
import com.service.UserService;
import com.service.UserServiceImpl;

public class LoginApp {

	public static void main(String[] args) {
		User user1 = new User();
		user1.setUserId(1);
		user1.setUserName("Akshaya");
		User user2 = new User();
		user2.setUserId(2);
		user2.setUserName("Ajay");
		User user3 = new User();
		user3.setUserId(3);
		user3.setUserName("Akash");

		List<User> users = new ArrayList<User>();
		users.add(user1);
		users.add(user2);
		users.add(user3);

		UserService userService = new UserServiceImpl();
		List<User> userList = userService.searchByUserId(users, 2);
		List<User> deleteUserById = userService.deletedByUserId(users, 1);

		if (userList.size() > 0) {
			for (User user : userList) {
				System.out.println("UserId:" + user.getUserId());
				System.out.println("UserNmae:" + user.getUserName());
			}
		}
		for (Iterator iterator = users.iterator(); iterator.hasNext();) {
			User user = (User) iterator.next();
			User updateUserName = userService.updateByUserId(user, 2);

			if (updateUserName != null) {

				System.out.println("After update UserID:" + updateUserName.getUserId());
				System.out.println("After update  UserName:" + updateUserName.getUserName());

			}
		}
		if (deleteUserById.size() > 0) {
			for (User user : deleteUserById) {
				System.out.println("After deleting UserIds:" + user.getUserId());
				System.out.println("After deleting UserNmaes:" + user.getUserName());
			}
		}

	}
}
