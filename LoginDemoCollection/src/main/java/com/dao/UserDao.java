package com.dao;

import java.util.List;


import com.model.User;

public interface UserDao {


	public abstract List<User> searchByUserId(List<User> user, int userId);
//	public abstract User searchByUserId(Set<User> user, int userId);

	public abstract User updateByUserId(User user, int userId);

	public abstract List<User>  deletedByUserId(List<User> user, int userId);

	

}
