package com.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.model.User;

public class UserDaoImpl implements UserDao {

	public List<User> searchByUserId(List<User> user, int userId) {
		List<User> tempList = new ArrayList<User>();
		for (Iterator iterator = user.iterator(); iterator.hasNext();) {
			User user2 = (User) iterator.next();
			if (user2.getUserId() == userId) {
				tempList.add(user2);

			}

		}
		return tempList;

	}

	public User updateByUserId(User user, int userId) {
		if (user.getUserId() == userId) {
			user.setUserName("Sai");
		}
		return user;

	}

	public List<User> deletedByUserId(List<User> user, int userId) {

		user.remove(userId);

		return user;

	}

}
