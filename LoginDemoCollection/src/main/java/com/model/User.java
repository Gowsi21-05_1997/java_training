package com.model;

public class User {
	private int userId;
	private String userName;

	public User() {

	}

	public User(int userId, String userName) {
	
		this.userId = userId;
		this.userName = userName;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + userId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		
		User other = (User) obj;
		if (userId != other.userId) {
			return false;
		}
		return true;
	}

	
	
	

}
