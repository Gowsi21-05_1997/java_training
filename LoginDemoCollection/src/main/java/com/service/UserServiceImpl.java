package com.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.dao.UserDao;
import com.dao.UserDaoImpl;
import com.model.User;

public class UserServiceImpl implements UserService {

	public List<User> searchByUserId(List<User> user, int userId) {
		List<User> list = new ArrayList<User>();
		UserDao userDao = new UserDaoImpl();
		list = userDao.searchByUserId(user, userId);
		return list;

	}

	public User updateByUserId(User user, int userId) {
		User temp = null;
		UserDao userDao = new UserDaoImpl();
		temp = userDao.updateByUserId(user, userId);
		return temp;

	}

	public List<User> deletedByUserId(List<User> user, int userId) {

		List<User> list = new ArrayList<User>();
		UserDao userDao = new UserDaoImpl();
		list = userDao.deletedByUserId(user, userId);
		return list;
	}

}
