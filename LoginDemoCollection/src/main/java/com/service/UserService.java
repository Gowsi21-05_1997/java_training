package com.service;

import java.util.List;
import java.util.Set;

import com.model.User;

public interface UserService {

	public abstract List<User> searchByUserId(List<User> user, int userId);
//	public abstract User searchByUserId(Set<User> user, int userId);

	public abstract User updateByUserId(User user, int userId);

	public abstract List<User>  deletedByUserId(List<User> user, int userId);

}
